#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include "cryptage.h"
#include "function.h"

int crytper(FILE *fic,FILE *fic2, wchar_t *lgLine, int choix2,int decalage, char cle[]){
    wchar_t ligne[256];
    lgLine = fgetws(ligne, 256, fic);
    while(lgLine) {
        char line[256] = "";
        if(desaccent(ligne,line)==1){

            //Crypte
            if(choix2==1){
                cesar(line,decalage);
            }else{
                vigenere(line,cle,1);
            }

            //Ecrit dans le fichier
            fprintf(fic2,"%s",line);
            lgLine = fgetws(ligne, 256, fic);
        }else{
            return 0;
        }
    }
    return 1;
}

int decryper(FILE *fic,FILE *fic2,int lgLigne, int choix2,int decalage, char cle[]){
    char* ligne = NULL;
    size_t taille = 0;
    lgLigne = getline(&ligne, &taille, fic);

    //Decrypter et ecrit dans le fichier créé tant qu'il reste une ligne à décrypter
    while (lgLigne != -1) {

        char line[taille];

        strcpy(line,ligne);

        //Decrypte
        if(choix2==1){
            cesar(line,-decalage);
        }else{
            vigenere(line,cle,2);
        }

        //Ecrit dans le fichier
        fputs(line,fic2);
        lgLigne = getline(&ligne, &taille, fic);
    }
    free(ligne);
    return 1;
}



//Ajout le decalage decal pour chaque lettre de ligne
void cesar(char ligne[], int decal) {
    int i = 0;
    while (ligne[i] != '\0') {
        decaler(ligne,i,decal,'A','Z');
        decaler(ligne,i,decal,'a','z');
        decaler(ligne,i,decal,'0','9');
        i++;
    }
}

void vigenere(char ligne[],char cle[],int choix){
    int i = 0;
    int j = 0;
    int decal;
    while (ligne[i] != '\0') {
        if(ligne[i] ==' '){
            i++;
        }
            
        //Calcul decal
        if(cle[j] >= 'a' && cle[j]<= 'z'){
           decal = cle[j]-'a';
        }else if (cle[j] >= 'A' && cle[j]<= 'Z'){
           decal = cle[j]-'A';
        }else if (cle[j] >= '0' && cle[j]<= '9'){
           decal = cle[j]-'0';
        }

        if(choix==2)
            decal = -decal;

        decaler(ligne,i,decal,'A','Z');
        decaler(ligne,i,decal,'a','z');
        decaler(ligne,i,decal,'0','9');

        i++;
        j++;
        if(j>= strlen(cle)){
           j=0;
        }
    }
}

void decaler(char *ligne, int indice, int cle, int borneMin, int borneMax){
    if (ligne[indice] >= borneMin && ligne[indice]<= borneMax) {
        int c = ligne[indice] - borneMin;
        c += cle;
        c = c % 26;
        if(c<0){
            c+=26;
        }
        ligne[indice] = c + borneMin;
    }
}