#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include "function.h"
#include "cryptage.h"


void main() {

    setlocale (LC_ALL,"fr_FR.utf8");

    //Choix pour crypter ou décrytper
    int choix;
    printf("Voulez vous : 1-Crypter         2-Décrypter         #-Arreter\n");
    scanf("%d",&choix);

    switch(choix){
        case 1:
            printf("Nom du fichier a crypter \n");
            break;
        case 2:
            printf("Nom du fichier a décryper\n");
            break;
        default:
            printf("Fin du programme\n");
            exit(EXIT_SUCCESS);
    }

    // Récupère le nom du premier fichier
    char nfic1[100];
    scanf("%s",nfic1);
    viderBuffer();

    // Récupère le nom du fichier à créer tant qu'il est différent du premier
    printf("Nom du fichier a creer\n");
    char nfic2[100];
    scanf("%s",nfic2);
    viderBuffer();
    while(strcmp(nfic2,nfic1)==0){
        printf("Veuillez choisir un nom different du fichier à crypter\n");
        scanf("%s",nfic2);
        viderBuffer();
    }

    FILE* fic;
    FILE* fic2;
    int lgLigne;
    wchar_t * lgLine;
    fic = fopen(nfic1, "r");

    if (fic != NULL) {
        fic2 = fopen(nfic2,"w");

        //Affiche les différents cryptages
        if(choix ==1){
            printf("Cryptage : 1-Cesar          2-Vigenere\n");
        }else{
            printf("Decryptage : 1-Cesar          2-Vigenere\n");
        }
        
        //Demande de choisir tant que c'est différent des choix donnés
        int choix2;
        scanf("%d",&choix2);
        while (choix2 != 1 && choix2 != 2){
            printf("Il n'y a pas d'autres méthodes. Veuillez choisir un des choix proposés.\n");
            printf("1-Cesar          2-Vigenere\n");
            scanf("%d",&choix2);
        }

        //Demande la clé
        char cle[100];
        int decalage;
        if (choix2 == 1){
            printf("Saisir decalage \n");
            scanf("%d",&decalage);
        }else{
            printf("Saisir mot cle \n");
            scanf("%s",cle);
            viderBuffer();
        }

        //Crypter
        if(choix==1){
            if(crytper(fic,fic2,lgLine,choix2,decalage,cle)==0){
                exit(EXIT_FAILURE);
            }
            
        //Decrypter            
        }else{
            decryper(fic,fic2,lgLigne,choix2,decalage,cle);
        } 
        fclose(fic);
        fclose(fic2);
    }else{
        printf("Il n'existe aucun fichier portant le nom %s\n",nfic1 );
        remove(nfic1);
    }
    printf("Le fichier a été crypté\n");
} 