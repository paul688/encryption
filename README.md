Sujet 4 Chiffrement des messages
Bînome : GILBERT Paul, HAUTECOEUR Enzo S2B.

L'objectif de l'application est de pouvoir traiter un fichier texte de l'utilisateur.
Il peut crypter ou décrypter. Nous laissons à l'utilisateur le choix du cryptage et sa clé.
Si on crypte, on vérifie la présence de caractères spéciaux (l'arrête si présence) puis enlève les accents pour ensuite le crypter.

Le fichier cryptage.c possède 5 fonctions :

	int crytper(FILE *fic,FILE *fic2, wchar_t *lgLine, int choix2,int decalage, char cle[]);
		En entrée, on prend le fichier "fic" qui est le fichier à crypter dans le fichier "fic2", la chaine de wchar_t "lgLine" pour vérifier s'il reste quelque chose à crypter. On prends également l'entier "choix2" qui pour savoir quel cryptage doit être utilisé et enfin les clés d"ecalage" "et "cle" pour pouvoir être utilisé dans le cryptage correspondant.

		La fonction retourne 0 s'il ya une erreur dans la fonction "desaccent", sinon elle crypte le texte de "fic" dans "fic2" et retourne 1


	int decryper(FILE *fic,FILE *fic2,int lgLigne, int choix2,int decalage, char cle[]);
		En entrée, on prend le fichier "fic" qui est le fichier à décrypter dans le fichier "fic2", l'entier "lgLigne" pour vérifier s'il reste quelque chose à crypter. On prends également l'entier "choix2" qui pour savoir quel décryptage doit être utilisé et enfin les clés "decalage" et "cle" pour pouvoir être utilisé dans le décryptage correspondant.

		La fonction retourne 1


	void decaler(char *ligne, int indice, int cle, int borneMin, int borneMax);
		En entrée la ligne à crypter, l'indice de la lettre à décaler, la clé("décalage" ou le "cle") et les bornes "borneMin" et "borneMax" pour savoir quand revenir au début en cas dépassement de bornes

		La fonction modifie le caratère en position "indice" de "ligne" avec le décalage "cle" et qu'il soit inclut entre les bornes


	void cesar(char ligne[], int decal);
		En entrée la ligne à crypter et le décalage à appliquer

		La fonction modifie la ligne grâce au décalage fait par la fonction decaler


	void vigenere(char ligne[],char cle[],int choix);
		En entrée la ligne à crypter et le décalage à appliquer

		La fonction modifie la ligne grâce au décalage fait par la fonction decaler. Pour choix = 1, on ajoute. Pour choix=2, on soustrait.



La fichier function.c comporte 3 fonctions :
	
	void viderBuffer(void);
		La fonction permet de vider le buffer stdin en cas de saisie de supérieur à 100 caractères


	int verifAlpha(wchar_t ligne[]){
		En entrée, la ligne à vérifier venant du fichier à crypter

		La fonction doit retourner 0 s'il y a un caratère spécial sinon elle retourne 1


	int desaccent(wchar_t ligne[], char line[]);
		En entrée la ligne "ligne" à crypter, line la ligne "ligne" sans accent

		La fonction retourne 0 si verifAlpha détecte une erreur sinon copie "ligne" en char et sans accent dans "line"